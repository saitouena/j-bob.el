;; j-bob: 't -> elisp: t
;; j-bob: 'nil -> elisp nil

;; my debug utils for dev
(defun run-tests ()
  (interactive)
  (eval-buffer)
  (ert-run-tests-interactively t))

;; j-bob
(defun list0 () ())
(defun list0? (x) (equal x ()))

(ert-deftest list0-test ()
  (should (list0? (list0))))

(defun list1 (x) (cons x (list0)))
(defun list1? (x) (if (atom x) nil (list0? (cdr x))))
(defun elem1 (xs) (car xs))

(ert-deftest list1-test ()
  (should (list1? (list1 'a)))
  (should (not (list1? (list0))))
  (let ((xs (list1 'a)))
    (should (equal (elem1 xs) 'a))))

(defun list2 (x y) (cons x (list1 y)))
(defun list2? (x) (if (atom x) nil (list1? (cdr x))))
(defun elem2 (xs) (elem1 (cdr xs)))

(ert-deftest list2-test ()
  "list2 test"
  (should (list2? (list2 'a 'b)))
  (should (not (list2? (list1 'a))))
  (let ((x (list2 'a 'b)))
    (should (equal (elem2 x) 'b))))

(defun list3 (x y z) (cons x (list2 y z)))
(defun list3? (x) (if (atom x) nil (list2? (cdr x))))
(defun elem3 (xs) (elem2 (cdr xs)))

(ert-deftest list3-test ()
  "list3 test"
  (should (list3? (list3 'a 'b 'c)))
  (should (not (list3? (list0))))
  (let ((x (list3 1 2 3)))
    (should (equal (elem3 x) 3))))

(defun tag (sym x) (cons sym x))
(defun tag? (sym x)
  (if (atom x) nil (equal (car x) sym)))
(defun untag (x) (cdr x))

(ert-deftest tag-test ()
  "tag test"
  (let ((if-expr (tag 'if '(Q A E)))
	(f-app (tag '+ '(1 2))))
    (should (tag? 'if if-expr))
    (should (tag? '+ f-app))
    (should (not (tag? 'if f-app)))
    (should (not (tag? '+ if-expr)))
    (should (equal (untag if-expr) '(Q A E)))
    (should (equal (untag f-app) '(1 2)))))

(defun member? (x ys)
  (if (atom ys)
      nil
    (if (equal x (car ys))
	t
      (member? x (cdr ys)))))

(ert-deftest member?-test ()
  (should (member? 1 '(1 2 3)))
  (should (member? 2 '(1 2 3)))
  (should (member? 3 '(1 2 3)))
  (should (not (member? 4 '(1 2 3)))))

(defun quote-c (value)
  (tag 'quote (list1 value)))
(defun quote? (x)
  (if (tag? 'quote x) (list1? (untag x)) nil))
(defun quote.value (e) (elem1 (untag e)))

(defun if-c (Q A E) (tag 'if (list3 Q A E)))
(defun if? (x)
  (if (tag 'if x) (list3? (untag x)) nil))
(defun if.Q (e) (elem1 (untag e)))
(defun if.A (e) (elem2 (untag e)))
(defun if.E (e) (elem3 (untag e)))

(defun app-c (name args) (cons name args))
(defun app? (x)
  (cond ((atom x) nil)
	((quote? x) nil)
	((if? x) nil)
	(t t)))
(defun app.name (e) (car e))
(defun app.args (e) (cdr e))

(defun var? (x)
  (cond ((equal x t) nil)
	((equal x nil) nil)
	((natnump x) nil) ;; natp = natnump in Elisp
	(t (atom x))))

(defun defun-c (name formals body)
  (tag 'defun (list3 name formals body)))
(defun defun? (x)
  (if (tag? 'defun x) (list3? (untag x)) nil))
(defun defun.name (def) (elem1 (untag def)))
(defun defun.formals (def) (elem2 (untag def)))
(defun defun.body (def) (elem3 (untag def)))

(defun dethm-c (name formals body)
  (tag 'dethm (list3 name formals body)))
(defun dethm? (x)
  (if (tag? 'dethm x) (list3? (untag x)) nil))
(defun dethm.name (def) (elem1 (untag def)))
(defun dethm.formals (def) (elem2 (untag def)))
(defun dethm.body (def) (elem3 (untag def)))

(defun if-QAE (e)
  (list3 (if.Q e) (if.A e) (if.E e)))
(defun QAE-if (es)
  (if-c (elem1 es) (elem2 es) (elem3 es)))

(defun rator? (name)
  "check if primitive. what does rator? stand for?"
  (member? name '(equal atom car cdr cons natp size + <)))

(defun rator.formals (rator)
  (if (member? rator '(atom car cdr natp))
      '(x)
    (if (member? (rator '(equal cons + <)))
	'(x y)
      'nil)))

(defun def.name (def)
  (if (defun? def)
      (defun.name def)
    (if (dethm? def)
	(dethm.name def)
      def)))

(defun def.formals (def)
  (if (dethm? def)
      (dethm.formals def)
    (if (defun? def)
	(defun.formals def)
      '())))

(defun if-c-when-necessary (Q A E)
  (if (equal A E) A (if-c Q A E)))

(defun conjunction (es)
  (if (atom es)
      t
    (if (atom (cdr es))
	(car es)
      (if-c (car es)
	    (conjunction (cdr es))
	    nil))))

(ert-deftest conjunction-test ()
  (should (equal (conjunction '(A B C))
		 '(if A (if B C nil) nil)))
  (should (equal (conjunction '(A)) 'A))
  (should (equal (conjunction '()) t)))

(defun implication (es e)
  (if (atom es)
      e
    (if-c (car es)
	  (implication (cdr es) e)
	  t)))

(ert-deftest implication-test ()
  (should (equal (implication '() 'C)
		 'C))
  (should (equal (implication '(A) 'B)
		 '(if A B t)))
  (should (equal (implication '(A B) 'C)
		 '(if A (if B C t) t))))

(defun lookup (name defs)
  (if (atom defs)
      name
    (if (equal (def.name (car defs)) name)
	(car defs)
      (lookup name (cdr defs)))))

(defun undefined? (name defs)
  (if (var? name)
      (equal (lookup name defs) name)
    nil))

(defun args-arity? (def args)
  "used by app-arity?"
  (if (dethm? def)
      nil
    (if (defun? def)
	(arity? (defun.formals def) args)
      (if (rator? def)
	  (arity? (rator.formals def) args)
	'nil))))

(defun app-arity? (defs app)
  "Check if the corrent number of args are supplied"
  (args-arity? (lookup (app.name app) defs) (app.args app)))

(defun bound? (var vars)
  (if (equal var 'any) t (member? var vars)))

(ert-deftest bound?-test ()
  (should (bound? 'x '(x y z)))
  (should (not (bound? 'z '(x y))))
  (should (bound? 'any '(x y z))))

;; TODO: add test for exprs?
(defun exprs? (defs vars es)
  "Check es with bound?, app-arity?"
  (if (atom es)
      t
    (let ((target (car es))
	  (rest (cdr es)))
      (cond ((var? target)
	     (if (bound? target vars)
		 (exprs? defs vars rest)
	       nil))
	    ((quote? target)
	     (exprs? defs vars rest))
	    ((if? target)
	     (if (exprs? defs vars (if-QAE target))
		 (exprs? defs vars rest)
	       nil))
	    ((app? target)
	     (if (app-arity? defs target)
		 (if (exprs? defs vars (app.args target))
		     (exprs? defs vars rest)
		   nil)
	       nil))))))

(defun expr? (def vars e)
  (exprs? defs vars (list1 e)))

;; set functions
(defun subset? (xs ys)
  (if (atom xs)
      t
    (if (member? (car xs) ys)
	(subset? (cdr xs) ys)
      nil)))

(defun list-extend (xs x)
  "appends x to the end of xs."
  (if (atom xs)
      (list1 x)
    (if (equal (car xs) x)
	xs
      (cons (car xs)
	    (list-extend (cdr xs) x)))))

(ert-deftest list-extend-test ()
  (should (equal (list-extend '(a b) 'c)
		 '(a b c))))

(defun list-union (xs ys)
  "hold the order from left to right."
  (if (atom ys)
      xs
    (list-union (list-extend xs (car ys))
		(cdr ys))))

(ert-deftest list-union-test ()
  (should (equal (list-union '(a b c d) '(x y z))
		 '(a b c d x y z))))

(defun get-arg-from (n args from)
  (if (atom args)
      nil
    (if (equal n from)
	(car args)
      (get-arg-from n (cdr args) (+ from 1)))))

(defun get-arg (n args)
  (get-arg-from n args 1))

(ert-deftest get-arg-test ()
  (should (equal (get-arg 1 '(a b c)) 'a))
  (should (equal (get-arg 3 '(a b c)) 'c)))

(defun set-arg-from (n args y from)
  (if (atom args)
      nil
    (if (equal n from)
	(cons y (cdr args))
      (cons (car args)
	    (set-arg-from n (cdr args) y (+ from 1))))))
(defun set-arg (n args y)
  (set-arg-from n args y 1))

(ert-deftest set-arg-test ()
  (should (equal (set-arg 1 '(a b c) 'd) '(d b c)))
  (should (equal (set-arg 3 '(a b c) 'd) '(a b d))))

(defun <=len-from (n args from)
  (if (atom args)
      nil
    (if (equal n from)
	t
      (<=len-from n (cdr args) (+ from 1)))))
(defun <=len (n args)
  (if (< 0 n) (<=len-from n args 1) nil))

(ert-deftest <=len-test ()
  (should (<=len 3 '(a b c)))
  (should-not (<=len 4 '(a b c)))
  (should (<=len 1 '(a b c)))
  (should-not (<=len 0 '(a b c))))

(defun arity? (vars es)
  "(= (length vars) (length es))"
  (if (atom vars)
      (atom es)
    (if (atom es)
	nil
      (arity? (cdr vars) (cdr es)))))

(defun formals? (vars)
  (cond ((atom vars) t)
	((var? (car vars))
	 (if (member? (car vars) (cdr vars))
	     nil
	   (formals? (cdr vars))))
	(t nil)))

(ert-deftest formals?-test ()
  (should (formals? '(a b c)))
  (should-not (formals? '(a b a))))

(defun direction? (dir)
  (if (natnump dir)
      t
    (member? dir '(Q A E))))

(ert-deftest direction?-test ()
  (should (direction? 1))
  (should (direction? 3))
  (should (direction? 'Q)))

(defun path? (path)
  (if (atom path)
      t
    (if (direction? (car path))
	(path? (cdr path))
      nil)))

(ert-deftest path?-test ()
  (should (path? '(1 A E)))
  (should-not (path? '(3 X P))))

(defun quoted-exprs? (args)
  (if (atom args)
      t
    (if (quote? (car args))
	(quoted-exprs? (cdr args))
      nil)))

(ert-deftest quoted-exprs?-test ()
  (should (quoted-exprs? '('1 '2 '3))))

(defun step-args? (defs def args)
  (if (dethm? def)
    (if (arity? (dethm.formals def) args)
      (exprs? defs 'any args)
      nil)
    (if (defun? def)
      (if (arity? (defun.formals def) args)
        (exprs? defs 'any args)
        nil)
      (if (rator? def)
        (if (arity? (rator.formals def) args)
          (quoted-exprs? args)
          nil)
        nil))))

(defun step-app? (defs app)
  (step-args? defs
    (lookup (app.name app) defs)
    (app.args app)))

(defun step? (defs step)
  (if (path? (elem1 step))
      (if (app? (elem2 step))
	  (step-app? defs (elem2 step))
	nil)
    nil))

(defun steps? (defs steps)
  (if (atom steps)
      t
    (if (step? defs (car steps))
	(steps? defs (cdr steps))
      nil)))

(defun induction-scheme-for? (def vars e)
  (if (defun? def)
    (if (arity? (defun.formals def) (app.args e))
      (if (formals? (app.args e))
        (subset? (app.args e) vars) ;; why subset? -> vars are dethms args. see seed?
        nil)
      nil)
    nil))

(ert-deftest induction-scheme-for?-test ()
  (should (induction-scheme-for?
	   (defun-c 'hoge '(x y) '(+ x y))
	   '(x y)
	   '(hoge x y)))
  (should-not (induction-scheme-for?
	       (defun-c 'hoge '(x y) '(+ x y))
	       '(x)
	       '(hoge x y)))
  (should (induction-scheme-for?
	   (defun-c 'hoge '(x y) '(+ x y))
	   '(x y z)
	   '(hoge x y))))

(defun induction-scheme? (defs vars e)
  (if (app? e)
      (induction-scheme-for?
       (lookup (app.name e) defs)
       vars
       e)
    nil))

(defun seed? (defs def seed)
  (if (equal seed 'nil)
    't
    (if (defun? def)
      (expr? defs (defun.formals def) seed)
      (if (dethm? def)
        (induction-scheme? defs
          (dethm.formals def)
          seed)
        'nil))))

(defun extend-rec (defs def)
  (if (defun? def)
    (list-extend defs
      (defun-c
        (defun.name def)
        (defun.formals def)
        (app-c (defun.name def)
          (defun.formals def))))
    defs))

(ert-deftest extend-rec-test ()
  (should (equal (extend-rec '() '(defun hoge (x y) (+ x y)))
		 '((defun hoge (x y) (hoge x y))))))

(defun def-contents? (known-defs formals body)
  (if (formals? formals)
    (expr? known-defs formals body)
    'nil))

(defun def? (known-defs def)
  (cond ((dethm? def)
	 (if (undefined? (dethm.name def)
			 known-defs)
	     (def-contents? known-defs
	       (dethm.formals def)
	       (dethm.body def))
	   'nil))
	((defun? def)
	 (if (undefined? (defun.name def)
			 known-defs)
	     (def-contents?
	       (extend-rec known-defs def) ;; add fun to known-defs
	       (defun.formals def)
	       (defun.body def))
	   'nil)
	 (t 'nil))))

(defun list2-or-more? (pf)
  (if (atom pf)
    'nil
    (if (atom (cdr pf))
      'nil
      't)))

(ert-deftest list2-or-more?-test ()
  (should (list2-or-more? '(a b)))
  (should-not (list2-or-more? '()))
  (should-not (list2-or-more? '(a)))
  (should (list2-or-more? '(a b c))))

(defun proof? (defs pf)
  (if (list2-or-more? pf)
    (if (def? defs (elem1 pf))
      (if (seed? defs (elem1 pf) (elem2 pf))
        (steps? (extend-rec defs (elem1 pf))
          (cdr (cdr pf)))
        'nil)
      'nil)
    'nil))
